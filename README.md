# RoboLinke

Robot for LinkedIn

## Install

1. `sudo npm i`
2.  For the program to work, you need to create the `config.json` file in the main directory.

Sample
`{
  "url": "https://www.linkedin.com/",
  "username": "email",
  "password": "123456"
}`

3. `node index`
